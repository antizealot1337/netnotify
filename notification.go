package netnotify

// Notification represents a notification and contains info about the origin as
// well as message info like title and message.
type Notification struct {
	Origin  string `json:"origin"`
	Title   string `json:"title"`
	Message string `json:"message"`
} //Notification

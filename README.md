# Netnotify

Netnotify allows notification to be broadcasted over the network so that they
can be displayed as notifications on other devices. Currently the other devices
are only Linux desktops.

Netnotify uses two programs to accomplish its goal.
1) The source program netnotify
2) The listener program netnotifyd

These programs mulitcast the notification data of UDP as a JSON string so any
information can be read by _anyone_.

## License
Licensed under the terms of the MIT license. See LICENSE file for more
information.

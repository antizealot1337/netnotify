package main

import (
	"encoding/json"
	"flag"
	"net"

	"bitbucket.org/antizealot1337/netnotify"
)

func main() {
	// The notification
	var notification netnotify.Notification

	// Parse the command line
	flag.StringVar(&notification.Origin, "origin", notification.Origin,
		"The origin for the notification")
	flag.StringVar(&notification.Title, "title", notification.Title,
		"The title for the notification")
	flag.StringVar(&notification.Message, "message", notification.Message,
		"The message for the notification")
	flag.Parse()

	// Marshal the notifcation
	data, err := json.Marshal(notification)
	if err != nil {
		panic(err)
	} //if

	// Resolve the addres
	addr, err := net.ResolveUDPAddr("udp", "224.0.0.1:4000")
	if err != nil {
		panic(err)
	} //if

	// Create the multicast udp connection
	conn, err := net.DialUDP("udp", nil, addr)
	if err != nil {
		panic(err)
	} //if
	defer conn.Close()

	// Write the data
	if _, err = conn.Write(data); err != nil {
		panic(err)
	} //if
} //main

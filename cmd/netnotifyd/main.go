package main

import (
	"encoding/json"
	"io"
	"log"
	"net"

	"bitbucket.org/antizealot1337/netnotify"
)

const (
	maxDGrameSize = 8192
)

func main() {
	// Resolve the UDP address
	addr, err := net.ResolveUDPAddr("udp", "224.0.0.1:4000")
	if err != nil {
		panic(err)
	} //if

	// Create the listener
	listener, err := net.ListenMulticastUDP("udp", nil, addr)
	if err != nil {
		panic(err)
	} //if
	defer listener.Close()

	// Create a buffer
	buff := make([]byte, maxDGrameSize)

	// The notification for marhsalling
	var n netnotify.Notification

	// Continuously loop
	for {
		// Read from the listener
		sread, src, err := listener.ReadFromUDP(buff)
		if err != nil && err != io.EOF {
			panic(err)
		} //if

		// Attempt to unmarshal the message
		if err = json.Unmarshal(buff[:sread], &n); err != nil {
			log.Printf("Error: (%v) from %s", err, src.String())
			continue
		} //if

		// Check if the origin is empty
		if n.Origin == "" {
			// Set the origin to the UDP source
			n.Origin = src.String()
		} //if

		// Handle the data
		if err = notify(n); err != nil {
			log.Printf("Error: (%v) from %s", err, src.String())
		} //if
	} //for
} //main

// +build !linux,!darwin

package main

import (
	"errors"

	"bitbucket.org/antizealot1337/netnotify"
)

func notify(notification netnotify.Notification) error {
	return errors.New("notify x error: unable to send desktop notifications")
} //handle

// +build darwin

package main

import (
	"fmt"
	"os/exec"

	"bitbucket.org/antizealot1337/netnotify"
)

func notify(notification netnotify.Notification) error {
	// Find the applescript executable
	exe, err := exec.LookPath("osascript")

	// Check for an error
	if err != nil {
		return fmt.Errorf("notify darwin error: %v", err)
	} //if

	// Create the executable
	cmd := exec.Command(exe, "-e", ascript(notification))

	// Run the command and check for an error
	if err := cmd.Run(); err != nil {
		return fmt.Errorf("notify darwin error: %v", err)
	} //if

	return nil
} //func

func ascript(n netnotify.Notification) string {
	return fmt.Sprintf(`display notification "%s @ %s" with title "%s"`,
		n.Title,
		n.Origin,
		n.Message)
} //func

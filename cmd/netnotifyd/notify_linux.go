// +build linux

package main

import (
	"fmt"
	"os/exec"

	"bitbucket.org/antizealot1337/netnotify"
)

func notify(notification netnotify.Notification) error {
	// Loopup notify-send command
	exe, err := exec.LookPath("notify-send")

	// Check for an error
	if err != nil {
		// Write the error
		return fmt.Errorf("notify linux error: %v", err)
	} //if

	// Create a command
	cmd := exec.Command(exe,
		notification.Title+" @ "+notification.Origin,
		notification.Message)

	// Check if we can notify the user via the notify command
	if err := cmd.Run(); err != nil {
		// Write the error
		return fmt.Errorf("notify linux error: %v", err)
	} //if

	return nil
} //func
